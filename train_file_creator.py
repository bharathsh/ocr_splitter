import pandas as pd 
from ast import literal_eval
import numpy as np 

def expand_attributes(df,add,url):
  # Remov the columns by expanding the dictionary
  df = pd.concat([df.drop('region_shape_attributes',1), df['region_shape_attributes'].apply(literal_eval).apply(pd.Series)],1)
  # Drop the unwanted coluimns 
  df = df.drop(['file_size', 'file_attributes', 'region_count', 'name'],1)
  # Create a url column by adding filename to the url
  df['url'] =url+df['#filename']
  # Set regions ID 
  df['region_id']+=add
  # Return dataframe 
  return df

def get_df(path,data_path,attr):
  annot = pd.read_csv(path)
  # Remove unwated files and extract the dictionary from csv get for via.html file 
  annots = expand_attributes(annot,0, data_path)
  # Change height and width to x and y coordinates 
  annots['y2'] = annots['y']+annots['height']
  annots['x2'] = annots['x']+annots['width']
  # Extract the number attribute 
  annots['number']= (annots['region_attributes'].apply(literal_eval).str[attr])
  # Drop the nans 
  annots = annots.dropna(subset=['number'])
  # Add 1 to care about the class_id 0 class_id is not recorded. 
  annots['number'] = annots['number'].astype(int)+1
  # Typecast the columns to int
  annots[['x', 'x2', 'y', 'y2','number']]= annots[['x', 'x2', 'y', 'y2','number']].astype(int)
  # Select the wanted columns
  ndf2 = annots[['url', 'x', 'x2', 'y', 'y2','number']]
  # Rename them for better understading 
  ndf2.columns = ['image_name', 'xmin', 'xmax', 'ymin', 'ymax', 'class_id']
  # Return the dataframe
  return ndf2


ndf2 = get_df('drive/My Drive/imgs/prop/via_region_data.csv','drive/My Drive/imgs/prop/','Number')
ndf3 = get_df('drive/My Drive/number_classifier/few_more.csv','drive/My Drive/number_classifier/few_more/','numbers')

ndf2 = pd.concat([ndf2,ndf3])

import os 
# Data Augemnted 
m = []
for i in ['.png','_25.png','_33.png','_50.png','_66.png','_75.png']:
  cpy = ndf2.copy()
  cpy['image_name'] = cpy['image_name'].str.split('.').str[0] + i
  m.append(cpy)

ndf2 = pd.concat(m)

# Save the image files in a variable to check if images are missing 
k = np.array(os.listdir('drive/My Drive/imgs/prop'))

k = k[k!='91.png']
# Remove the rows were the images are not present 
ndf2 = ndf2[ndf2['image_name'].str.split('/').str[-1].isin(k) ]

# Save the train and test file.
from sklearn.model_selection import train_test_split
X_tr_names,X_te_names = train_test_split(ndf2['image_name'].unique(),shuffle=True,test_size=0.1)
X_tr = ndf2[ndf2['image_name'].isin(X_tr_names)]
X_te = ndf2[ndf2['image_name'].isin(X_te_names)]

X_tr.to_csv('train.csv',index=None)
X_te.to_csv('test.csv',index=None)

from __future__ import division

import os
import pickle
from io import BytesIO
import numpy as np
from tensorflow import keras
from PIL import Image
from base64 import b64decode
from boxer import Boxer
import sys
import cv2
from timeit import default_timer as timer
import numpy as np

np.set_printoptions(precision=2, suppress=True, linewidth=90)

class Predictor(object):

    def __init__(self, model):
        self.model = model
    
    # def get_beginning_and_end_idx(self,nm):
      
    #   # Make yellow intensive black 
    #   v = abs(nm - np.array([[[127, 172, 189]]]))
      
    #   # Adjust to the mean 
    #   v[v<50] = 0 
    #   # Get the sum of the colors vetically 
    #   vals = v[:,:(int(v.shape[1]/2))].sum(0)
    #   vals[vals<75] = 99999999
    #   # Find the minimum sum index across three dimensions and get the max across the RGB 
    #   bidx = max(vals.argmin(0))
    #   # Make white intensize black

    #   w = abs(nm - np.array([[[255, 255, 255]]]))
    #   # Find the sum of all the pixesl vertically  
    #   vals = w[:,(int(v.shape[1]/2)):].sum(0)
    #   # Get the ending index but taking the max across the dimesions and the minimum sum  
    #   eidx = (int(w.shape[1]/2))+max(vals.argmin(0))
    #   return bidx,eidx    
    def get_beginning_and_end_idx(self,image):  
        ratio = 1000/image.shape[1]
        width = int(image.shape[1]*ratio)
        height = int(image.shape[0]*ratio)
        image = cv2.resize(image,(width,height)).copy()
        img = image.copy()
        
        kernel = np.ones((3,3),np.uint8)
        img[:,:,0] = cv2.dilate(img[:,:,0],kernel,iterations = 5)
        img[:,:,1] = cv2.dilate(img[:,:,1],kernel,iterations = 5)
        img[:,:,2] = cv2.dilate(img[:,:,2],kernel,iterations = 5)

        hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

        yellow_lower = (20, 50, 80)
        yellow_upper = (30, 255, 255)

        mask_yellow = cv2.inRange(hsv, yellow_lower, yellow_upper)
        stidx = mask_yellow.sum(0).argsort()[-100:][::-1]

        bidx = int(stidx.max())
        
        eidx = int(stidx.max() + ((stidx.max() - stidx.min())*3))

        return image,bidx,eidx

    def predict(self, path, **kwargs):
        # Open image
        img = cv2.imread(path)
        # Get the beginning and ending index of the bill - y axix 
        img,bidx,eidx = self.get_beginning_and_end_idx(img)
        div = 10         
        # Get the bill 
        img = img[:,bidx:eidx]
        # cv2.imwrite(f'demo_{path.split("/")[-1].split(".")[0]}.png',img)

        # Get the segment size based on image size 
        # print(img.shape[1]/img.shape[0],img.shape[1],img.shape[0])
       
        seg_size = int(img.shape[0]/div)
        # print(img.shape)
        # For each segment run OCR.
        numbers = []
        for i in range(0,img.shape[0],seg_size):
            # Get the segment of image 
            n_img = img[i:i+seg_size,:]
            # print(n_img.shape)
            cv2.imwrite(f'{path.split("/")[-1].split(".")[0]}_{i}.png',n_img)
            # Get the OCR of the segment and append it to list of numbers 
            numbers.append(self.model.get_boxes(n_img))
        # Set done state to 0 
        done = 0

        k_number = '000000000000'
        # print([''.join(i) for i in numbers])
        for i in numbers:
          # Filter out the groups 
          # print(''.join(i))
          if ((len(i)>9) and (len(i)<15)) and (i[0]=='2') :
            k_number = ''.join(i)
            # print('K Number:',k_number)
            done = 1
            break
        # if done == 0 :
        #   print('K number not detected')
        # Return the K Number  
        return  k_number

    @classmethod
    def from_path(cls, model_dir):
        """Creates an instance of Predictor using the given path.

        This loads artifacts that have been copied from model directory 
        Predictor uses them during prediction.

        Args:
            model_dir: The local directory that contains the trained Keras
                model
        Returns:    
            An instance of `MyPredictor`.
        """
        model = Boxer(model_dir)
    
        return cls(model)



from difflib import SequenceMatcher

def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

def main():

    # img_path = sys.argv[1]
    # Initialize class
    op = Predictor.from_path('model_files')
    probs = []
    start = timer()
    w_names = []
    nw_names = []
    for i in os.listdir('/home/niki/test_set/new_test_set'):
      if ('.' in i) and ('.pdf' not in i) and ('.zip' not in i):
        filename = f'/home/niki/test_set/new_test_set/{i}'
        preds = op.predict(filename)

        true = i.split('.')[0].split('_')[-1]
        prob = similar(preds,true)
        print('True:',true, "Predicted:", preds , 'Similarity:',prob )
        
        probs.append(prob)
        if prob > 0.5:
          w_names.append(filename)
        else:
          nw_names.append(filename)


    print(probs)
    print(w_names)
    print(nw_names)
    print('Average:',np.array(probs).sum()/len(probs))  
    g5 = np.array(probs)
    
    print('Average:',g5[g5>0.5].sum()/(g5>0.5).sum())

    end = timer()
    print(end - start)    


if __name__== "__main__":
    main()
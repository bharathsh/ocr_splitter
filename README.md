## Usage: 

#### If you want to just try out an example 

1. Clone the repository 
2. Prerequisites: 

 - 'opencv-python', 
 - 'Pillow',  
 - 'scikit-image', 
 - 'tqdm',
 - 'keras==2.2.4'

3. Command line example :

```
python3 predictor.py example_input/1.png
```

#### Inside a python script 

```
from predictor import Predictor 
pred = Predictor.from_path(model_dir)
knumber = pred.predict(img_path) 
```

## How does it work?

### Steps involved in finding the k number 


- Bill is isolated from the background

    - We will find the yellow intensive column of the image and set that column as the beginning index in the first half of the image
	
    - We will find the white intensize column of the image and set that column as the ending index in the second half of the image 

    - Now based on the begining and ending index we isolate the bill
	
- Now the isolated bill is divided into different segments of same size and each segements of the bill is passed to SSD model to detect the numbers. 
	
- The numbers returned from SSD model are stored in a list
	
- Based on the condition that the number is between 10-15 digits and starts with 2 we filter out the number and return the k number 

### Steps involved in adding the bounding boxes 

- The segment of image is resized to 160x800
 
- The image is passed to SSD model and the bounding box co ordinates are returned 

- We will keep only the highest number of the boxes at different heights 

- We will remove the overlapping by grouping the boxes based on difference between the  median size of the all the boxes and keep the box with highest probability 

- The number detected is returned to the predictor  

### Flow Diagram

![image](https://drive.google.com/uc?export=download&id=134BuO3tZyY--LC2AzKBm-jPqjCIDJZFq)
